<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200831090935 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE games (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, difficulty INTEGER NOT NULL, created_at DATETIME NOT NULL, score INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_FF232B31A76ED395 ON games (user_id)');
        $this->addSql('CREATE TABLE options (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, question_id INTEGER NOT NULL, option_en VARCHAR(255) NOT NULL, option_fr VARCHAR(255) NOT NULL, is_correct BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_D035FA871E27F6BF ON options (question_id)');
        $this->addSql('CREATE TABLE questions (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, theme_id INTEGER NOT NULL, user_id INTEGER DEFAULT NULL, question_en VARCHAR(255) NOT NULL, question_fr VARCHAR(255) NOT NULL, difficulty INTEGER NOT NULL, state BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_8ADC54D559027487 ON questions (theme_id)');
        $this->addSql('CREATE INDEX IDX_8ADC54D5A76ED395 ON questions (user_id)');
        $this->addSql('CREATE TABLE questions_asked (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, game_id INTEGER NOT NULL, question_id INTEGER DEFAULT NULL, answered BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_8855288CE48FD905 ON questions_asked (game_id)');
        $this->addSql('CREATE INDEX IDX_8855288C1E27F6BF ON questions_asked (question_id)');
        $this->addSql('CREATE TABLE themes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, theme_en VARCHAR(255) NOT NULL, theme_fr VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE titles (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title_fr VARCHAR(255) NOT NULL, title_en VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, last_activity DATETIME NOT NULL, xp INTEGER DEFAULT NULL, api_token VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497BA2F5EB ON user (api_token)');
        $this->addSql('CREATE TABLE user_titles (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title_id INTEGER NOT NULL, user_id INTEGER NOT NULL, state BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_A4175A9EA9F87BD ON user_titles (title_id)');
        $this->addSql('CREATE INDEX IDX_A4175A9EA76ED395 ON user_titles (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE games');
        $this->addSql('DROP TABLE options');
        $this->addSql('DROP TABLE questions');
        $this->addSql('DROP TABLE questions_asked');
        $this->addSql('DROP TABLE themes');
        $this->addSql('DROP TABLE titles');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_titles');
    }
}
