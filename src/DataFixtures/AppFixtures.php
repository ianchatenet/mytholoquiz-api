<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Entity\Questions;
use App\Entity\Themes;
use App\Entity\Options;
use App\Entity\Titles;
use App\Entity\UserTitles;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

     public function __construct(UserPasswordEncoderInterface $passwordEncoder)
     {
         $this->passwordEncoder = $passwordEncoder;
     }



    public function load(ObjectManager $manager)
    {

        function createQuestions($Question,$options,$user,$manager)
        {
            $question = new Questions();
            $question->setQuestionEn($Question["En"]);
            $question->setQuestionFr($Question["Fr"]);
            $question->setDifficulty($Question["dif"]);
            $question->setTheme($Question["theme"]);
            $question->setUser($user);
            $question->setState($Question["state"]);
            $manager->persist($question);

            
            for($i=0; $i<count($options); $i++){
                $option= new Options();
                $option->setoptionEn($options[$i]["En"]);
                $option->setoptionFr($options[$i]["Fr"]);
                $option->setIsCorrect($options[$i]["cor"]);
                $option->setQuestion($question);
                $manager->persist($option);
            }
        }

        $user = new User(); 
        $user->setEmail("ian.chatenet.simplon@gmail.com");
        $user->setUsername("Ian");
        $user->setPassword($this->passwordEncoder->encodePassword(
           $user,
           "motDePasse"
        ));
        $user->setApiToken("ian");
        $user->setRoles(["ROLE_ADMIN"]);
        $manager->persist($user);

        $title = new Titles();
        $title->setTitleFr("Métèque");
        $title->setTitleEn("Metic");
        $manager->persist($title);

        $usTi = new UserTitles();
        $usTi->setTitle($title);
        $usTi->setUser($user);
        $usTi->setState(false);
        $manager->persist($usTi);

        $title = new Titles();
        $title->setTitleFr("Barbare");
        $title->setTitleEn("Barbaric");
        $manager->persist($title);

        $title = new Titles();
        $title->setTitleFr("Admin");
        $title->setTitleEn("Admin");
        $manager->persist($title);

        $usTi = new UserTitles();
        $usTi->setTitle($title);
        $usTi->setUser($user);
        $usTi->setState(true);
        $manager->persist($usTi);

        $user = new User(); 
        $user->setEmail("test@test.fr");
        $user->setUsername("test");
        $user->setPassword($this->passwordEncoder->encodePassword(
           $user,
           "testpasse"
        ));
        $user->setRoles(["ROLE_MODO"]);
        $user->setApiToken("test");
        $manager->persist($user);


        $theme = new Themes();
        $theme->setThemeEn("Greek");
        $theme->setThemeFr("Grecque");
        $manager->persist($theme);

        $question=[
            'En'=>"Wich god was born with an armor and a spear ?",
            'Fr'=>"Quel dieu est née avec une armur et une lance ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>false,
        ];

        $option1=["En"=>"Athena","Fr"=>"Athéna","cor"=>true,];
        $option2=["En"=>"Ares","Fr"=>"Ares","cor"=>false,];
        $option3=["En"=>"Alala","Fr"=>"Alala","cor"=>false,];
        $option4=["En"=>"Bia","Fr"=>"Bia","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//1

        $question=[
            'En'=>"What is the name of the Cyclops that Odysseus faced ?",
            "Fr"=>"Comment s'appelle le cyclope que Odysseus a affronté ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"Polyphemus","Fr"=>"Polyphème","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//2

        $question=[
            'En'=>"Which god represent death ?",
            'Fr'=>"Quel dieu represente la mort ?",
            "dif"=>3,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Hypnos","Fr"=>"Hypnos","cor"=>false,];
        $option2=["En"=>"Hades","Fr"=>"Hadès","cor"=>false,];
        $option3=["En"=>"Thanathos","Fr"=>"Thanathos","cor"=>true,];
        $option4=["En"=>"Persephone","Fr"=>"Perséphone","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//3

        $question=[
            'En'=>"Who is the inventor of the labyrinth of King Minos ?",
            "Fr"=>"Qui est l'inventeur du labyrinthe du roi Minos ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"Daedalus","Fr"=>"Dédale","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//4

        $question=[
            'En'=>"Who is Jason looking for the Golden Fleece with ?",
            'Fr'=>"Avec qui Jason cherche-t-il la Toison d'Or ?",
            "dif"=>3,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"The astronauts","Fr"=>"Les astronautes","cor"=>false,];
        $option2=["En"=>"The argonauts","Fr"=>"Les argonautes","cor"=>true,];
        $option3=["En"=>"The taikonauts","Fr"=>"Les taïkonautes","cor"=>false,];
        $option4=["En"=>"The vyomanauts","Fr"=>"Les vyomanautes","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];
        createQuestions($question,$options,$user,$manager);//5

        $question=[
            'En'=>"Medusa was one of the three ...",
            "Fr"=>"Méduse était une des trois ...",
            "dif"=>1,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"Gorgons","Fr"=>"Gorgones","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//6

        $question=[
            'En'=>"Who freed the city of Thebes from the sphinx ?",
            'Fr'=>"Qui libéra la ville de Thèbes du sphinx ?",
            "dif"=>3,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Achilles","Fr"=>"Achille","cor"=>false,];
        $option2=["En"=>"Heracles","Fr"=>"Héraclès","cor"=>false,];
        $option3=["En"=>"Agamemnon","Fr"=>"Agamemnon","cor"=>false,];
        $option4=["En"=>"Oedipe","Fr"=>"Oedipe","cor"=>true];
        $options=[$option1,$option2,$option3,$option4];
        createQuestions($question,$options,$user,$manager);//7

        $question=[
            'En'=>"Who killed the chimera ?",
            "Fr"=>"Qui tua la chimère ?",
            "dif"=>1,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"Bellerophon","Fr"=>"Bellérophon","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//8

        $question=[
            'En'=>"What is the power of the Aphrodite belt ?",
            'Fr'=>"Quel est le pouvoir de la ceinture d'Aphrodite ?",
            "dif"=>3,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Makes the wearer irresistible","Fr"=>"Rend le porteur irrésistible","cor"=>true,];
        $option2=["En"=>"Gives strength","Fr"=>"Donne de la force","cor"=>false,];
        $option3=["En"=>"Makes invisible","Fr"=>"Rend invisible","cor"=>false,];
        $option4=["En"=>"Tighten the pants","Fr"=>"Serre les pantalons","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//9

        $question=[
            'En'=>"What are the names of the soldiers accompanying Achilles ?",
            "Fr"=>"Quel est le nom des soldats qui accompagne Achille ?",
            "dif"=>1,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"The myrmidons","Fr"=>"Les myrmidons","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//10

        $theme = new Themes();
        $theme->setThemeEn("Norse");
        $theme->setThemeFr("Nordique");
        $manager->persist($theme);


        $question=[
            'En'=>"Who is the AllFather ?",
            'Fr'=>"Qui est le père de tout ?",
            "dif"=>3,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Jörmungandr ","Fr"=>"Jörmungandr","cor"=>false,];
        $option2=["En"=>"Thor","Fr"=>"Thor","cor"=>false,];
        $option3=["En"=>"Odin","Fr"=>"Odin","cor"=>true,];
        $option4=["En"=>"Sleipnir","Fr"=>"Sleipnir","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//1

        $question=[
            'En'=>"What is the name of the cosmic tree that supports the nine worlds ?",
            "Fr"=>"Comment appelle-t-on l'arbre cosmique qui soutient les neuf mondes ?",
            "dif"=>1,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"Yggdrasil","Fr"=>"Yggdrasil","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//2

        $question=[
            'En'=>"In Norse mythology, the kingdom of men is called ...",
            'Fr'=>"Dans la mythologie nordique, le royaume des hommes est appelé...",
            "dif"=>1,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Asgard","Fr"=>"Asgard","cor"=>false,];
        $option2=["En"=>"Midgard","Fr"=>"Midgard","cor"=>true,];
        $option3=["En"=>"Vanaheim","Fr"=>"Vanaheim","cor"=>false,];
        $option4=["En"=>"Midland","Fr"=>"Midland","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//3

        $question=[
            'En'=>"What is the name of the bridge between the worlds ?",
            "Fr"=>"Quel est le nom du pont entre les mondes ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"Bifrost","Fr"=>"Bifrost","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//4

        $question=[
            'En'=>"What is the name of the giants's world ?",
            'Fr'=>"Quel est le nom du monde des géants ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Asgard","Fr"=>"Asgard","cor"=>false,];
        $option2=["En"=>"Midgard","Fr"=>"Midgard","cor"=>false,];
        $option3=["En"=>"Vanaheim","Fr"=>"Vanaheim","cor"=>false,];
        $option4=["En"=>"Jötunheim","Fr"=>"Jötunheim","cor"=>true];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//5

        $question=[
            'En'=>"Where do warriors who died in battle go ?",
            "Fr"=>"Où se rendent les guerriers mort au combat ?",
            "dif"=>1,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"In Valhalla","Fr"=>"Au Valhalla","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//6

        $question=[
            'En'=>"What are the Eddas ?",
            'Fr'=>"Que sont les Eddas ?",
            "dif"=>3,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Poems relating Nordic myths","Fr"=>"Des poèmes relatant les mythes nordique","cor"=>true,];
        $option2=["En"=>"A people of Norse mythology","Fr"=>"Un peuple de la mythologie nordique","cor"=>false,];
        $option3=["En"=>"Evil creatures","Fr"=>"Des créatures maléfique","cor"=>false,];
        $option4=["En"=>"The names of the gods living in Asgard","Fr"=>"Le nom des dieux vivant en Asgard","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//7

        $question=[
            'En'=>"What animal is Fenrir ?",
            "Fr"=>"Quel animal est Fenrir ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"A wolf","Fr"=>"Un loup","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//8

        $question=[
            'En'=>"What did Höd the blind god use to kill Baldr ?",
            'Fr'=>"Qu'a utilisé Höd le dieu aveugle pour tuer Baldr ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"A magic bow and arrows","Fr"=>"Un arc et des flèches magique","cor"=>false,];
        $option2=["En"=>"Mjöllnir","Fr"=>"Mjöllnir","cor"=>false,];
        $option3=["En"=>"A branch of mistletoe","Fr"=>"Une branche de gui","cor"=>true,];
        $option4=["En"=>"Gungnir","Fr"=>"Gungnir","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//9

        $question=[
            'En'=>"Who is the guardian of the bifrost ?",
            "Fr"=>"Qui est le gardien du bifrost ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];
        $options=[["En"=>"Heimdall","Fr"=>"Heimdall","cor"=>true]];
        createQuestions($question,$options,$user,$manager);//10
        

        $theme = new Themes();
        $theme->setThemeEn("Mongolian");
        $theme->setThemeFr("Mongol");
        $manager->persist($theme);

        $question=[
            'En'=>"What is the name of Heavenly-Father ?",
            'Fr'=>"Quel est le nom du Ciel-Père ?",
            "dif"=>2,
            "theme"=>$theme,
            "state"=>true,
        ];

        $option1=["En"=>"Tengri","Fr"=>"Tengri","cor"=>true,];
        $option2=["En"=>"Khan","Fr"=>"Khan","cor"=>false,];
        $option3=["En"=>"Etügen","Fr"=>"Etügen","cor"=>false,];
        $option4=["En"=>"Begtsé","Fr"=>"Begtsé","cor"=>false];
        $options=[$option1,$option2,$option3,$option4];

        createQuestions($question,$options,$user,$manager);//1

        $manager->flush();
    }

    
}
