<?php

namespace App\Entity;

use App\Repository\QuestionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=QuestionsRepository::class)
 */
class Questions
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"quiz"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({"quiz"})
     * @Groups({"gamesPublic"})
     */
    private $questionEn;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({"quiz"})
     * @Groups({"gamesPublic"})
     */
    private $questionFr;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"quiz"})
     * @Groups({"gamesPublic"})
     */
    private $difficulty;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"admin","adminTheme"})
     */
    private $state;

    /**
     * @ORM\OneToMany(targetEntity=Options::class, mappedBy="question", orphanRemoval=true)
     * @Groups({"quiz"})
     * @Groups({"gamesPublic"})
     */
    private $options;

    /**
     * @ORM\ManyToOne(targetEntity=Themes::class, inversedBy="questions")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"quiz"})
     */
    private $theme;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="questions")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"admin"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=QuestionsAsked::class, mappedBy="question", orphanRemoval=true)
     */
    private $questionsAskeds;

    public function __construct()
    {
       
        $this->options = new ArrayCollection();
        $this->questionsAskeds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestionEn(): ?string
    {
        return $this->questionEn;
    }

    public function setQuestionEn(string $questionEn): self
    {
        $this->questionEn = $questionEn;

        return $this;
    }

    public function getQuestionFr(): ?string
    {
        return $this->questionFr;
    }

    public function setQuestionFr(string $questionFr): self
    {
        $this->questionFr = $questionFr;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }
    
    public function getState(): ?bool
    {
        return $this->state;
    }

    public function setState(bool $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|Options[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(Options $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
            $option->setQuestion($this);
        }

        return $this;
    }

    public function removeOption(Options $option): self
    {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
            // set the owning side to null (unless already changed)
            if ($option->getQuestion() === $this) {
                $option->setQuestion(null);
            }
        }

        return $this;
    }

    public function getTheme(): ?Themes
    {
        return $this->theme;
    }

    public function setTheme(?Themes $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|QuestionsAsked[]
     */
    public function getQuestionsAskeds(): Collection
    {
        return $this->questionsAskeds;
    }

    public function addQuestionsAsked(QuestionsAsked $questionsAsked): self
    {
        if (!$this->questionsAskeds->contains($questionsAsked)) {
            $this->questionsAskeds[] = $questionsAsked;
            $questionsAsked->setQuestion($this);
        }

        return $this;
    }

    public function removeQuestionsAsked(QuestionsAsked $questionsAsked): self
    {
        if ($this->questionsAskeds->contains($questionsAsked)) {
            $this->questionsAskeds->removeElement($questionsAsked);
            // set the owning side to null (unless already changed)
            if ($questionsAsked->getQuestion() === $this) {
                $questionsAsked->setQuestion(null);
            }
        }

        return $this;
    }
}
