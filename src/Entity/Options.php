<?php

namespace App\Entity;

use App\Repository\OptionsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OptionsRepository::class)
 */
class Options
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"quiz"})
     * @Groups({"gamesPublic"})
     */
    private $optionEn;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"quiz"})
     * @Groups({"gamesPublic"})
     */
    private $optionFr;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"quiz"})
     * @Groups({"gamesPublic"})
     */
    private $isCorrect;

    /**
     * @ORM\ManyToOne(targetEntity=questions::class, inversedBy="options")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOptionEn(): ?string
    {
        return $this->optionEn;
    }

    public function setOptionEn(string $optionEn): self
    {
        $this->optionEn = $optionEn;

        return $this;
    }

    public function getOptionFr(): ?string
    {
        return $this->optionFr;
    }

    public function setOptionFr(string $optionFr): self
    {
        $this->optionFr = $optionFr;

        return $this;
    }

    public function getIsCorrect(): ?bool
    {
        return $this->isCorrect;
    }

    public function setIsCorrect(bool $isCorrect): self
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    public function getQuestion(): ?questions
    {
        return $this->question;
    }

    public function setQuestion(?questions $question): self
    {
        $this->question = $question;

        return $this;
    }
}
