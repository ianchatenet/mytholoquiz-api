<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"userPublic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * @Groups({"userProfil","admin"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"userProfil","admin"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "Your password must be at least {{ limit }} characters long",
     *      allowEmptyString = false
     * )
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=true,
     *     message="Your password cannot contain a number"
     * )
     * 
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Groups({"userPublic","admin"})
     */
    private $username;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"userProfil","admin"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"userPublic"})
     */
    private $lastActivity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"userPublic"})
     */
    private $xp;

    /**
     * @ORM\OneToMany(targetEntity=Games::class, mappedBy="user", orphanRemoval=true)
     * @Groups({"userPublic"})
     */
    private $games;


    /**
     * @ORM\OneToMany(targetEntity=Questions::class, mappedBy="user")
     * @Groups({"userProfil"})
     */
    private $questions;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     * @Groups({"userProfil"})
     */
    private $apiToken;

    /**
     * @ORM\OneToMany(targetEntity=UserTitles::class, mappedBy="User", orphanRemoval=true)
     * @Groups({"userPublic"})
     */
    private $userTitles;
    

    public function __construct()
    {
        
        $this->games = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->userTitles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getLastActivity(): ?\DateTimeInterface
    {
        return $this->lastActivity;
    }

    public function setLastActivity(\DateTimeInterface $lastActivity): self
    {
        $this->lastActivity = $lastActivity;

        return $this;
    }


    /**
    * @ORM\PrePersist
    */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->lastActivity = new \Datetime();
    }

    public function getXp(): ?int
    {
        return $this->xp;
    }

    public function setXp(?int $xp): self
    {
        $this->xp = $xp;

        return $this;
    }


    /**
     * @return Collection|Games[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Games $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->setUser($this);
        }

        return $this;
    }

    public function removeGame(Games $game): self
    {
        if ($this->games->contains($game)) {
            $this->games->removeElement($game);
            // set the owning side to null (unless already changed)
            if ($game->getUser() === $this) {
                $game->setUser(null);
            }
        }

        return $this;
    }

    

    /**
     * @return Collection|Questions[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Questions $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setUser($this);
        }

        return $this;
    }

    public function removeQuestion(Questions $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getUser() === $this) {
                $question->setUser(null);
            }
        }

        return $this;
    }

    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    public function setApiToken(?string $apiToken): self
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * @return Collection|UserTitles[]
     */
    public function getUserTitles(): Collection
    {
        return $this->userTitles;
    }

    public function addUserTitle(UserTitles $userTitle): self
    {
        if (!$this->userTitles->contains($userTitle)) {
            $this->userTitles[] = $userTitle;
            $userTitle->setUser($this);
        }

        return $this;
    }

    public function removeUserTitle(UserTitles $userTitle): self
    {
        if ($this->userTitles->contains($userTitle)) {
            $this->userTitles->removeElement($userTitle);
            // set the owning side to null (unless already changed)
            if ($userTitle->getUser() === $this) {
                $userTitle->setUser(null);
            }
        }

        return $this;
    }

  

}
