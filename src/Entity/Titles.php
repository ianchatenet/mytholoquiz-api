<?php

namespace App\Entity;

use App\Repository\TitlesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TitlesRepository::class)
 */
class Titles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"userPublic"})
     */
    private $titleFr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"userPublic"})
     */
    private $titleEn;

    /**
     * @ORM\OneToMany(targetEntity=UserTitles::class, mappedBy="Title", orphanRemoval=true)
     */
    private $userTitles;

    public function __construct()
    {
        $this->userTitles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleFr(): ?string
    {
        return $this->titleFr;
    }

    public function setTitleFr(string $titleFr): self
    {
        $this->titleFr = $titleFr;

        return $this;
    }

    /**
     * @return Collection|UserTitles[]
     */
    public function getUserTitles(): Collection
    {
        return $this->userTitles;
    }

    public function addUserTitle(UserTitles $userTitle): self
    {
        if (!$this->userTitles->contains($userTitle)) {
            $this->userTitles[] = $userTitle;
            $userTitle->setTitle($this);
        }

        return $this;
    }

    public function removeUserTitle(UserTitles $userTitle): self
    {
        if ($this->userTitles->contains($userTitle)) {
            $this->userTitles->removeElement($userTitle);
            // set the owning side to null (unless already changed)
            if ($userTitle->getTitle() === $this) {
                $userTitle->setTitle(null);
            }
        }

        return $this;
    }

    public function getTitleEn(): ?string
    {
        return $this->titleEn;
    }

    public function setTitleEn(string $titleEn): self
    {
        $this->titleEn = $titleEn;

        return $this;
    }
   
}
