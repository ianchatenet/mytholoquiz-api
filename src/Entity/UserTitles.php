<?php

namespace App\Entity;

use App\Repository\UserTitlesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserTitlesRepository::class)
 */
class UserTitles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"userPublic"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Titles::class, inversedBy="userTitles")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"userPublic"})
     */
    private $Title;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userTitles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"userPublic"})
     */
    private $state;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?Titles
    {
        return $this->Title;
    }

    public function setTitle(?Titles $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getState(): ?bool
    {
        return $this->state;
    }

    public function setState(bool $state): self
    {
        $this->state = $state;

        return $this;
    }
}
