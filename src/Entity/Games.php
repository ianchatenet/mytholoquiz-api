<?php

namespace App\Entity;

use App\Repository\GamesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=GamesRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Games
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"gamesPublic"})
     */
    private $difficulty;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"gamesPublic"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"gamesPublic","userProfil"})
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=QuestionsAsked::class, mappedBy="game", orphanRemoval=true)
     * @Groups({"gamesPublic","userProfil"})
     */
    private $questionsAskeds;

    public function __construct()
    {
        $this->questionsAskeds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|QuestionsAsked[]
     */
    public function getQuestionsAskeds(): Collection
    {
        return $this->questionsAskeds;
    }

    public function addQuestionsAsked(QuestionsAsked $questionsAsked): self
    {
        if (!$this->questionsAskeds->contains($questionsAsked)) {
            $this->questionsAskeds[] = $questionsAsked;
            $questionsAsked->setGame($this);
        }

        return $this;
    }

    public function removeQuestionsAsked(QuestionsAsked $questionsAsked): self
    {
        if ($this->questionsAskeds->contains($questionsAsked)) {
            $this->questionsAskeds->removeElement($questionsAsked);
            // set the owning side to null (unless already changed)
            if ($questionsAsked->getGame() === $this) {
                $questionsAsked->setGame(null);
            }
        }

        return $this;
    }


    /**
    * @ORM\PrePersist
    */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }
}
