<?php

namespace App\Entity;

use App\Repository\QuestionsAskedRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=QuestionsAskedRepository::class)
 */
class QuestionsAsked
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"gamesPublic"})
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"gamesPublic","userProfil"})
     */
    private $answered;

    /**
     * @ORM\ManyToOne(targetEntity=games::class, inversedBy="questionsAskeds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity=Questions::class, inversedBy="questionsAskeds")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"gamesPublic"})
     */
    private $question;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnswered(): ?bool
    {
        return $this->answered;
    }

    public function setAnswered(bool $answered): self
    {
        $this->answered = $answered;

        return $this;
    }

    public function getGame(): ?games
    {
        return $this->game;
    }

    public function setGame(?games $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getQuestion(): ?Questions
    {
        return $this->question;
    }

    public function setQuestion(?Questions $question): self
    {
        $this->question = $question;

        return $this;
    }


}
