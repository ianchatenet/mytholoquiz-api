<?php

namespace App\Entity;

use App\Repository\ThemesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=ThemesRepository::class)
 */
class Themes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"quiz","adminTheme"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"quiz","adminTheme"})
     */
    private $themeEn;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"quiz","adminTheme"})
     */
    private $themeFr;

    /**
     * @ORM\OneToMany(targetEntity=Questions::class, mappedBy="theme")
     * @Groups({"adminTheme"})
     */
    private $questions;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    

  

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThemeEn(): ?string
    {
        return $this->themeEn;
    }

    public function setThemeEn(string $themeEn): self
    {
        $this->themeEn = $themeEn;

        return $this;
    }

    public function getThemeFr(): ?string
    {
        return $this->themeFr;
    }

    public function setThemeFr(string $themeFr): self
    {
        $this->themeFr = $themeFr;

        return $this;
    }

    /**
     * @return Collection|Questions[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Questions $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setTheme($this);
        }

        return $this;
    }

    public function removeQuestion(Questions $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getTheme() === $this) {
                $question->setTheme(null);
            }
        }

        return $this;
    }

}
