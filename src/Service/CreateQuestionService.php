<?php

namespace App\Service;

use App\Repository\ThemesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Questions;
use App\Entity\Options;

class CreateQuestionService{

    private $themeRepo;
    private $manager;

    public function __construct(ThemesRepository $themeRepo, EntityManagerInterface $manager)
    {
        $this->themeRepo = $themeRepo;
        $this->manager = $manager;
    }

    function createQuestion($user,$params, $context){

            $manager = $this->manager;
            $theme= $this->themeRepo->findOneBy(["id"=>$params["theme"]]);
            $question = new Questions;
            $question->setQuestionFr($params["questionFr"]);
            $question->setQuestionEn($params["questionEn"]);
            $question->setTheme($theme);
            $question->setDifficulty($params["difficulty"]);
            $question->setUser($user);

            if($context === "modo"){
                $question->setState(false);
            }

            if($context === "admin"){
                $question->setState($params["state"]);
            }

        foreach($params["options"] as $optionParams){
            $option = new Options;
            $option->setOptionFr($optionParams["optionFr"]);
            $option->setOptionEn($optionParams["optionEn"]);
            $option->setIsCorrect($optionParams["isCorrect"]);
            $option->setQuestion($question);
            $manager->persist($option);
        }
        
        $manager->persist($question);
        $manager->flush();
        return $question;   
    }
}