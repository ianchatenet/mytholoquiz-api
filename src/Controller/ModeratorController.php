<?php 
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\CreateQuestionService;
use App\Repository\ThemesRepository;


/**
 * @Route("/api/modo")
 */
class ModeratorController extends AbstractController{

    /**
     * @Route("/question",name="modoQuestion",methods="POST")
     */
    function createQuestion(Request $request,ThemesRepository $themeRepo, CreateQuestionService $createQuestS){
        $params = json_decode($request->getContent(),true)["question"];
        $manager = $this->getDoctrine()->getManager();
        $user = $user = $this->getUser();
        $question = $createQuestS->createQuestion($user,$params, "modo");

        return $this->json($question,201,[],["groups"=>["quiz"]]);
    }
}