<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Repository\TitlesRepository;
use App\Entity\UserTitles;


/**
 * @Route("/api")
 */
class UserController extends AbstractController
{

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/users", name="users", methods={"GET"})
     */
    public function index(UserRepository $userRepo)
    {
        return $this->json($userRepo->findAll(),200,[],["groups"=>"userPublic"]);
    }

    /**
     * @Route("/user/{id}", name="user", methods={"GET"})
     */
    public function show($id, UserRepository $userRepo)
    {
        return $this->json($userRepo->find($id),200,[],["groups"=>"userPublic"]);
    }

    /**
     * @Route("/register",name="register",methods={"POST"})
     */
    public function create(Request $request, TitlesRepository $titlesrepo){

        $manager = $this->getDoctrine()->getManager();
        $params = json_decode($request->getContent(), true);

        

       try{
        $user = new User(); 
        $user->setEmail($params["email"]);
        $user->setUsername($params["username"]);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $params["password"]
        ));
        $manager->persist($user);

        $title = $titlesrepo->findOneBy(["titleEn"=>"Barbaric"]);
        $userTitle = new UserTitles();
        $userTitle->setTitle($title);
        $userTitle->setUser($user);
        $userTitle->setState(true);
        $manager->persist($userTitle);

        $manager->flush();
        
        return $this->json($user,200,[],["groups"=>["userPublic","userProfil"]]);

       } 
       catch(\Exception $e){
           $error = $e->getMessage();
           return $this->json($e,500);
       }
    }
}
