<?php
namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ThemesRepository;


/**
 * @Route("/mytholoquiz")
 */
class PublicController extends AbstractController{


    /**
     * @Route("/themes",name="getThemes",methods="GET")
     */
    function getThemes(ThemesRepository $themesRepo){

        return $this->json($themesRepo->findAll(),200,[],["groups"=>["quiz"]]);
    }
}