<?php
namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\OptionsRepository;
use App\Repository\QuestionsRepository;
use App\Repository\ThemesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\CreateQuestionService;
use App\Entity\Options;
use App\Entity\Questions;
use App\Entity\Themes;


/**
 * @Route("/api/admin")
 */
class AdminController extends AbstractController{

    /**
     * @Route("/questions",name="adminQuestions",methods="GET")
     */
    function getQuestions(QuestionsRepository $questRepo){
        $questions = $questRepo->findAll();

        return $this->json($questions,200,[],["groups"=>["admin","quiz"]]);
    }

    /**
     * @Route("/question",name="deleteQuestion",methods="DELETE")
     */
    function deleteQuestion(QuestionsRepository $questRepo, Request $request){
        $params = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();
        $question = $questRepo->findOneBy(["id"=>$params["questionId"]]);
        $manager->remove($question);
        $manager->flush();
        return $this->json($question,200,[],["groups"=>["admin","quiz"]]);
    }

    /**
     * @Route("/question",name="updateQuestion",methods="PUT")
     */
    function updateQuestion(QuestionsRepository $questRepo,OptionsRepository $optionsRepo, Request $request){
        $params = json_decode($request->getContent(),true)["questions"];
        $manager = $this->getDoctrine()->getManager();
        $question = $questRepo->findOneBy(["id"=>$params["id"]]);
        $options = $optionsRepo->findBy(["question"=>$question]);
        foreach($options as $option){
            $manager->remove($option);
        }
        foreach($params["options"] as $optionParams){
            $option = new Options;
            $option->setOptionFr($optionParams["optionFr"]);
            $option->setOptionEn($optionParams["optionEn"]);
            $option->setIsCorrect($optionParams["isCorrect"]);
            $option->setQuestion($question);
            $manager->persist($option);
        }
        $question->setQuestionFr($params["questionFr"]);
        $question->setQuestionEn($params["questionEn"]);
        $question->setDifficulty($params["difficulty"]);
        $question->setState($params["state"]);
        $manager->persist($question);
        $manager->flush();
        return $this->json($question,200,[],["groups"=>["admin","quiz"]]);
    }
    
    /**
     * @Route("/question",name="createQuestion",methods="POST")
     */
    function createQuestion(Request $request,CreateQuestionService $createQuestS){
        $params = json_decode($request->getContent(),true)["question"];
        $manager = $this->getDoctrine()->getManager();

        $user = $user = $this->getUser();
        $question = $createQuestS->createQuestion($user,$params,"admin");

        return $this->json($question,201,[],["groups"=>["quiz","admin"]]);
    }


    /**
     * @Route("/users",name="adminUsers",methods="GET")
     */
    function showUsers(UserRepository $userRepo){
        return $this->json($userRepo->findAll(),200,[],["groups"=>["userPublic","admin"]]);
    }

    /**
     * @Route("/user",name="updateUser",methods="PUT")
     */
    function updateUser(UserRepository $userRepo, Request $request){
        $params = json_decode($request->getContent(),true)["user"];
        $manager = $this->getDoctrine()->getManager();
        $user =$userRepo->findOneBy(["id"=>$params["id"]]);

        $user->setRoles($params["roles"]);
        $manager->persist($user);
        $manager->flush();

        return $this->json($user,200,[],["groups"=>["userPublic","admin"]]);
    }

    /**
     * @Route("/themes",name="adminThemes",methods="GET")
     */
    function showThemes(ThemesRepository $themesRepo){
        return $this->json($themesRepo->findAll(),200,[],["groups"=>["adminTheme"]]);
    }

    /**
     * @Route("/theme",name="createTheme",methods="POST")
     */
    function createThemes(Request $request){
        $params = json_decode($request->getContent(),true)["theme"];
        $manager = $this->getDoctrine()->getManager();

        $theme = new Themes();
        $theme->setThemeFr($params["themeFr"]);
        $theme->setThemeEn($params["themeEn"]);

        $manager->persist($theme);
        $manager->flush();

        return $this->json($theme,201,[],["groups"=>"adminTheme"]);
    }

    /**
     * @Route("/theme",name="updateTheme",methods="PUT")
     */
    function updateThemes(ThemesRepository $themeRepo,Request $request){
        $params = json_decode($request->getContent(),true)["theme"];
        $manager = $this->getDoctrine()->getManager();
        $theme = $themeRepo->findOneBy(["id"=>$params["id"]]);
        $theme->setThemeFr($params["themeFr"]);
        $theme->setThemeEn($params["themeEn"]);
        $manager->persist($theme);
        $manager->flush();

        return $this->json($theme,200,[],["groups"=>["adminTheme"]]);
    }

    /**
     * @Route("/theme",name="deleteTheme",methods="DELETE")
     */
    function deleteTheme(Themesrepository $themesRepo, Request $request){
        $params = json_decode($request->getContent(),true);
        $manager = $this->getDoctrine()->getManager();
        $theme = $themesRepo->findOneBy(["id"=>$params["id"]]);
        $manager->remove($theme);
        $manager->flush();

        return $this->json($theme,200,[],["groups"=>"adminTheme"]);
    }
}