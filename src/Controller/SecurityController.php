<?php 
// src/Controller/SecurityController.php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
* @Route("/api")
*/
class SecurityController extends AbstractController
{

    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
    * @Route("/login", name="login", methods={"POST"})
    */
    public function login(UserRepository $userRepo, Request $request)
    {
         $params = json_decode($request->getContent(), true);
         $user = $userRepo->findOneBy(["email"=>$params["email"]]);
         if($user){
            $pass = $this->passwordEncoder->isPasswordValid($user, $params['password']);
            if($user->getPassword()==$pass){
                $manager = $this->getDoctrine()->getManager();
                $token = str_shuffle ( "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ); 
                $user->setApiToken($token);
                $user->setLastActivity(new \DateTime());
                $manager->persist($user);
                $manager->flush();
                return $this->json($user,200,[],["groups"=>["userPublic","userProfil"]] );
             }
         }
        return $this->json("bad credentials",400);
    }

    /**
    * @Route("/logout",name="logout", methods={"POST"})
    */
    public function logout(Request $request, UserRepository $userRepo){
        $params = json_decode($request->getContent(), true);
        $user = $userRepo->findOneBy(["email"=>$params["email"]]);
        $manager = $this->getDoctrine()->getManager();
        $token = str_shuffle ("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ); 
        $user->setApiToken($token);
        $manager->persist($user);
        $manager->flush();
        return $this->json("logged out",200);
    }
}
