<?php 
// src/Controller/SecurityController.php

namespace App\Controller;

use App\Repository\QuestionsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Games;
use App\Entity\QuestionsAsked;
use App\Repository\UserRepository;
use App\Repository\GamesRepository;

/**
* @Route("/api")
*/
class QuizController extends AbstractController
{

    /**
     * @Route("/createGame",name="createGame",methods="POST")
     */
    public function createGame(Request $request, UserRepository $userRepo, QuestionsRepository $questRepo){
        $manager = $this->getDoctrine()->getManager();
        $params = json_decode($request->getContent(), true);
        
        $user = $userRepo->findOneBy(["id"=>$params["user"]]);
        $xp = ($user->getXp()+$params["score"]+10)/10;
        $user->setXp($xp);
        $game = new Games();
        $game->setScore($params["score"]);
        $game->setDifficulty(2);
        $game->setUser($user);
        $manager->persist($game);

        foreach($params["questions"] as $question){
            $questionOrigin = $questRepo->findOneBy(["id"=>$question[0]]);
            $questionAsked = new QuestionsAsked();
            $questionAsked->setAnswered($question[1]);
            $questionAsked->setQuestion($questionOrigin);
            $questionAsked->setGame($game);
            $manager->persist($questionAsked);
        }
        $manager->flush();
        return $this->json($game,200,[],["groups"=>"gamesPublic"]);
    }

    /**
     * @Route("/getGames",name="getGames",methods="GET")
     */
    public function getGames(GamesRepository $gamesRepo)
    {
        return $this->json($gamesRepo->findAll(),200,[],["groups"=>"gamesPublic"]);
    }
    
    /**
    * @Route("/quiz", name="quiz",methods="GET")
    */
    public function quiz(QuestionsRepository $questRepo)
    {
        return $this->json($questRepo->quiz(),200,[],["groups"=>"quiz"]);

    }
}
