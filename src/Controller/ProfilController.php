<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use App\Repository\UserTitlesRepository;


/**
 * @Route("/api/profil")
 */
class ProfilController extends AbstractController{
    
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    /**
     * @Route("/update",name="update",methods={"PUT"})
     */
    public function update(Request $request,UserTitlesRepository $titlesRepo){
            $manager = $this->getDoctrine()->getManager();
            $params = json_decode($request->getContent(), true)["user"];
            $user = $user = $this->getUser();
            $errors=[];

            if($params["username"]){
                $user->setUsername($params["username"]);
            }
            
            if($params["email"]){
                $user->setEmail($params["email"]);
            }

            if($params['ancientPassword']!==""){
                $pass = $this->passwordEncoder->isPasswordValid($user, $params['ancientPassword']);

                if($user->getPassword()!=$pass){
                    array_push($errors,"wrong password");
                    return $this->json($errors,403,[],["groups"=>["userPublic","userProfil"]]);
                }
                
                $user->setPassword($this->passwordEncoder->encodePassword(
                    $user,
                    $params["newPassword"]
                ));
            }

            if($params["title"]!==""){
                $currentTitle= $titlesRepo->findOneBy(["state"=>true,"User"=>$user]);
                $currentTitle->setState(false);
                $manager->persist($currentTitle);

                $newTitle = $titlesRepo->findOneBy(["id"=>$params['title']]);
                $newTitle->setState(true);
                $manager->persist($newTitle);
            }
            
            $manager->persist($user);
            $manager->flush();
            return $this->json($user,200,[],["groups"=>["userPublic","userProfil"]]);
    }

    /**
     * @Route("/refresh",name="refresh",methods="GET")
     */
    public function refresh(){
        $user= $this->getUser();
        return $this->json($user,200,[],["groups"=>["userPublic","userProfil"]]);
    }

    /**
     * @Route("/delete",name="delete",methods={"DELETE"})
     */
    public function delete( UserRepository $userRepo){
        $id = $user = $this->getUser()->getId();
        $user = $userRepo->findOneBy(["id"=>$id]);
        $session = new Session();
        $session->invalidate();
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($user);
        $manager->flush();
        return $this->json("user deleted",200);
    }
}