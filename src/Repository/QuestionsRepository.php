<?php

namespace App\Repository;

use App\Entity\Questions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Questions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Questions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Questions[]    findAll()
 * @method Questions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Questions::class);
    }

    public function quiz(){
        $response = [];

         $ids = $this->createQueryBuilder('q')
                    ->where("q.state = true")
                    ->select('q.id')
                    ->getQuery()
                    ->getResult();
        shuffle($ids);

        for($i=0;$i<2;$i++){
            $entity=$this->createQueryBuilder('q')
                ->where("q.id = :id")
                ->setParameter(':id', $ids[$i]["id"])
                ->getQuery()                    ->getResult();
            array_push($response,$entity[0]);

        }
        return $response;
    }

    //SQL request does not work like we want for now
    function test(){
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT option_en, option_fr,question_fr,question_en, state 
        From options 
        LEFT JOIN questions ON question_id = questions.id
        WHERE questions.state LIKE :true
        GROUP BY questions.id
        ORDER BY random(); 
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['true' => true]);
        dd( $stmt->fetchAll());
        return $stmt->fetchAll();
    }
}
