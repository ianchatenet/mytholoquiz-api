<?php

namespace App\Repository;

use App\Entity\UserTitles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserTitles|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTitles|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTitles[]    findAll()
 * @method UserTitles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserTitlesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTitles::class);
    }

    // /**
    //  * @return UserTitles[] Returns an array of UserTitles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserTitles
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
