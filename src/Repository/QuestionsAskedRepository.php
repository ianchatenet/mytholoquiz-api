<?php

namespace App\Repository;

use App\Entity\QuestionsAsked;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuestionsAsked|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionsAsked|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionsAsked[]    findAll()
 * @method QuestionsAsked[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionsAskedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuestionsAsked::class);
    }

    // /**
    //  * @return QuestionsAsked[] Returns an array of QuestionsAsked objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionsAsked
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
